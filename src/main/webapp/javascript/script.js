$(document).ready(function(){
    $('#calcular').click(function(){
        var isValid = true;
        if($('#numero').val() == "" || !$.isNumeric($('#numero').val())){
            isValid = false
        }
        $('#mensajes').empty();
        if(isValid){
            BRUJULA.primefactors.calculate($('#numero').val());
        	$("#mensajes").append("<li>Los factores primos de " + $('#numero').val() + " son:</li>");
        } else {
            $("#mensajes").append("<li class='error'>El número no es válido</li>");
        }
    });
});